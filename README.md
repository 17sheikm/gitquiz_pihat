# GitQuiz_pihat
# Description
The system allows a user to connect the system to a heater power cable(via the onboard
relay) for example and allows the user to specify an exact temperature which allows
extremely customised temperature control in contrast to adjusting the temperature with a
control knob as with most generic heaters. This allows for a room temperature to be decently
maintained
# Instructions
After the PiHat is attached to the Pi - an external device such as a heating element can interface with the pi. In order to connect the element - one terminal wire of the element is cut in two and each end is connected into a relay on the PiHat which then switches on and off to connect od disconnect the contact between the wires (which turns the device on and off). The Led indicators will display the system temperature status which can be set manually. 
